package controller;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;

import model.NestedLoop;
import view.SoftwareGui;

public class Testing {
	private SoftwareGui frame;
	private NestedLoop model;
	private ActionListener list;
	
	public static void main(String[] args) {
		new Testing();
	}
	
	public Testing(){
		
		model = new NestedLoop();
		frame = new SoftwareGui();
		
		frame.pack();
		frame.setVisible(true);
        frame.setSize(600,370);
        frame.getContentPane().setLayout(new GridLayout(0,2));
        frame.setListener(list);

        frame.setListener(new ActionListener() {
	        public void actionPerformed(ActionEvent event){
	        	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				System.exit(0);		   	               	
	         }            
		});
        
        frame.setListener2(new ActionListener() {
	        public void actionPerformed(ActionEvent event){
	        	
	        	String t1 ="Nested Loop Type 1";
	        	if (t1 == frame.type.getSelectedItem()){
	        		int input = Integer.parseInt(frame.getInput());
	        		frame.setResult(model.Type1(input));
	        	}
	        	
	        	String t2 ="Nested Loop Type 2";
	        	if (t2 == frame.type.getSelectedItem()){
	        		int input = Integer.parseInt(frame.getInput());
	        		frame.setResult(model.Type2(input));
	        	}
	        	
	        	String t3 ="Nested Loop Type 3";
	        	if (t3 == frame.type.getSelectedItem()){
	        		int input = Integer.parseInt(frame.getInput());
	        		frame.setResult(model.Type3(input));
	        	}
	        	String t4 = "Nested Loop Type 4";
	        	if (t4 == frame.type.getSelectedItem()){
	        		int input = Integer.parseInt(frame.getInput());
	        		frame.setResult(model.Type4(input));
	        	}
	        	
	        	String t5 = "Nested Loop Type 5";
	        	if (t5 == frame.type.getSelectedItem()){
	        		int input = Integer.parseInt(frame.getInput());
	        		frame.setResult(model.Type5(input));
	        	}
	         }      
		});
        
        
	}
}
