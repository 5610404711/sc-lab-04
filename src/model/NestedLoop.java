package model;

public class NestedLoop {
	private String str;
	
	public String Type1(int n){
		String str = "";
		for(int i = 1 ; i<= n; i++){
			for(int j = 1; j<= n; j++)
				str = str + "*";
			str = str + "\n";
		}
	return str;
	}
	
	public String Type2(int n){
		String str = "";
		for(int i = 1 ; i<= n; i++){
			for(int j = 1; j<= n; j++)
				str = str + "*";
			str = str + "\n";
		}
	return str;
	}
	
	public String Type3(int n){
		String str = "";
		for(int i = 1; i <= n; i++){
			for (int j = 1; j <= i; j++)
				str = str + "*";
			str = str + "\n";
		}
	return str;
	}
	
	public String Type4(int n){
		String str = "";
		for(int i =1; i <= n ; i++){
			for(int j = 1; j<= n; j++)
				if (j%2 == 0){
					str = str + "*";
				}
				else {
					str = str +"-";
				}
		str = str + "\n";
		}
		return str;
	}
	
	public String Type5(int n){
		String str = "";
		for(int i = 1; i <= n; i++){
			for(int j =1; j<= n; j++){
				if((i+j)%2 == 0){
					str = str + "*";
				}
				else {
					str = str + " ";
				}
			}
		str = str + "\n";
		}
		return str;
	}
	
}
